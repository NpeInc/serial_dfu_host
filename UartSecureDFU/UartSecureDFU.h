//  Copyright (c) 2017-2020 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
#pragma once
#include <stdbool.h> 
#ifndef _UART_SECURE_DFU
#define _UART_SECURE_DFU


#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

typedef enum
{
	SUCCESS = 0,
	BAD_COM_PORT_OR_FILE = 1,
	ERROR_OPENING_PORT = 2,
	ERROR_SENDING_FILE = 3,
	ERROR_CLOSING_PORT = 4
} error_code_t;

typedef enum
{
	APP					= 0,
	BL					= 1,
	SD					= 2,
	SD_AND_BL			= 3,
	NEW_BL_ACTIVATION	= 4,
	UNKNOWN				= 5
} transfer_type_t;

#if defined(WIN32)
__declspec(dllexport) typedef void(__stdcall* ProgressCallback)(int);
__declspec(dllexport) typedef void(__stdcall* TransferTypeCallback)(int);
__declspec(dllexport) int do_serial_dfu(char* comPort, char* zipPkgName, bool isUSBTransport, ProgressCallback progressCallback, TransferTypeCallback transferCallback);

#else
typedef void (*ProgressCallback)(int);
typedef void (*TransferTypeCallback)(int);
int do_serial_dfu(char* comPort, char* zipPkgName, bool isUSBTransport, ProgressCallback progressCallback, TransferTypeCallback transferCallback);
#endif


#ifdef __cplusplus
}   /* ... extern "C" */
#endif  /* __cplusplus */


#endif // _UART_SECURE_DFU
