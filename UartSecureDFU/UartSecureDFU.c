//  Copyright (c) 2017-2020 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
#include <stdio.h>
#include <string.h>
#include "uart_drv.h"
#include "uart_slip.h"
#include "dfu.h"
#include "logging.h"
#include "UartSecureDFU.h"
#include "npe_log.h"




#if defined(IS_APP)
static int is_argv_verbose(char *p_argv)
{
	int err_code;

	if (!strcmp(p_argv, "-v") || !strcmp(p_argv, "-V"))
		err_code = 0;
	else
		err_code = 1;

	return err_code;
}

void fakeProgressCallabck(int progress)
{
	LOGI("Progress %d",progress)
}

void fakePhaseCallabck(int phase)
{
	LOGI("Phase %d", phase);
}

int main(int argc, char *argv[])
{
	int err_code = 0;
	int show_usage = 0;
	char *portName = NULL;
	uart_drv_t uart_drv;
	char *zipName = NULL;
	int argn;
	int info_lvl = LOGGER_INFO_LVL_0;

	if (argc >= 2 && strlen(argv[1]) > 0)
		portName = argv[1];
	else
	{
		show_usage = 1;
		err_code = 1;
	}

	if (argc >= 3 && strlen(argv[2]) > 0)
		zipName = argv[2];
	else
	{
		show_usage = 1;
		err_code = 1;
	}

	for (argn = 3; argn < argc && !err_code; argn++)
	{
		err_code = is_argv_verbose(argv[argn]);
		if (!err_code)
		{
			info_lvl++;

			logger_set_info_level(info_lvl);

			if (info_lvl >= LOGGER_INFO_LVL_3)
				break;
		}
		else
		{
			show_usage = 1;
			err_code = 1;

			break;
		}
	}

	if (show_usage)
	{
		printf("Usage: UartSecureDFU serial_port package_name [-v] [-v] [-v]\n");
	}

	uart_drv.p_PortName = portName;

	if (!err_code)
	{
		err_code = uart_slip_open(&uart_drv, true);
	}

	if (!err_code)
	{
		dfu_param_t dfu_param;

		dfu_param.p_uart = &uart_drv;
		dfu_param.p_pkg_file = zipName;
		//err_code = dfu_send_package(&dfu_param);
		err_code = dfu_send_package(&dfu_param, NULL, true, NULL);
		if(err_code)
			return BAD_COM_PORT_OR_FILE;
		
	}

	if (!show_usage)
	{
		int err_code2 = uart_slip_close(&uart_drv);
		
		if (!err_code)
			err_code = err_code2;
	}

	return err_code;
}


#endif

#if defined(WIN32)
__declspec(dllexport) int 
#else
int
#endif
do_serial_dfu(char* comPort, char* zipPkgName, bool isUSBTransport, ProgressCallback progressCallback, TransferTypeCallback transferCallback)
{
	int err_code = SUCCESS;
	char* portName = NULL;
	uart_drv_t uart_drv;
	char* zipName = NULL;
	int info_lvl = LOGGER_INFO_LVL_0;

	if (comPort != NULL && strlen(comPort) > 0)
		portName = comPort;
	else
	{
		err_code = BAD_COM_PORT_OR_FILE;
	}

	if (zipPkgName != NULL && strlen(zipPkgName) > 0)
		zipName = zipPkgName;
	else
	{
		err_code = BAD_COM_PORT_OR_FILE;
	}

	logger_set_info_level(LOGGER_INFO_LVL_3);

	uart_drv.p_PortName = portName;

	if (err_code == SUCCESS)
	{
		// open com port (returns err_code of 2 if unable to open com port)
		uart_slip_close(&uart_drv); // Ignore error
		err_code = uart_slip_open(&uart_drv, isUSBTransport);
		LOGI("Open Port %s", uart_drv.p_PortName);
		if (err_code)
		{

			err_code = ERROR_OPENING_PORT;

		} 
	}

	if (err_code == SUCCESS)
	{
		dfu_param_t dfu_param;

		dfu_param.p_uart = &uart_drv;
		dfu_param.p_pkg_file = zipName;
		err_code = dfu_send_package(&dfu_param, progressCallback, isUSBTransport, transferCallback);
		//if (err_code) err_code = ERROR_SENDING_FILE;
	}

	int err_code2 = uart_slip_close(&uart_drv);
	if (err_code2) err_code2 = ERROR_CLOSING_PORT;

	if (err_code == SUCCESS)
	{
		err_code = err_code2;
	}

	return err_code;
}